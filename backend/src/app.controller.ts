import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  PayloadTooLargeException,
  Post,
  Put,
  Req,
} from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('/food')
  createFoodEntry(@Body() payload, @Req() req) {
    return this.appService.createFoodEntry(payload, req.user);
  }

  @Put('/food/:id')
  updateFoodEntry(@Param('id') id: number, @Body() payload, @Req() req) {
    return this.appService.updateFoodEntry(id, payload, req.user);
  }

  @Delete('/food/:id')
  deleteFoodEntry(@Param('id') id: number, @Req() req) {
    return this.appService.deleteFoodEntry(id, req.user);
  }

  @Get('/food')
  getAllFood() {
    return this.appService.getAllFood();
  }

  @Get('/food/:id')
  getSingleFood(@Param('id') id: string) {
    return this.appService.getSingleFood(id);
  }
}
