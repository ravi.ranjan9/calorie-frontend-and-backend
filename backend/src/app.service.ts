import { HttpException, Injectable, NotFoundException } from '@nestjs/common';
import FoodEntity from './food/food.entity';
import UserEntity from './food/user.entity';
import { validateFoodEntry } from './joi.validators';

@Injectable()
export class AppService {
  async getSingleFood(id: string) {
    const food = await FoodEntity.findOne(id);
    if (!food) {
      throw new NotFoundException('Could not find Entry');
    }
    return { ...food };
  }

  async getAllFood() {
    const allFood = await FoodEntity.find();
    return { ...allFood };
  }

  async createFoodEntry(
    payload: {
      date: string;
      name: string;
      calorie: number;
      price: number;
    },
    authUser: UserEntity,
  ) {
    const { value, error } = validateFoodEntry(payload);
    if (error) {
      throw new HttpException(error.message, 400);
    }
    const { date, name, calorie, price } = value;
    const food = new FoodEntity();
    food.id = Math.random();
    food.name = name;
    food.price = price;
    food.calorie = calorie;
    food.date = date;
    food.userId = authUser.id;
    await food.save();
    console.log('Food entry created');
  }

  async updateFoodEntry(
    id: number,
    payload: { date: string; name: string; calorie: number; price: number },
    authUser: UserEntity,
  ) {
    const food = await FoodEntity.findOne(id);
    if (food) {
      if (food.userId === authUser.id || authUser.isAdmin) {
        const { value, error } = validateFoodEntry(payload);
        if (error) {
          throw new HttpException(error.message, 400);
        }
        const { date, name, calorie, price } = value;
        food.id = id;
        food.name = name;
        food.calorie = calorie;
        food.date = date;
        food.price = price;
        food.userId = authUser.id;
        await food.save();
      } else {
        throw new HttpException('Forbidden Access', 403);
      }
    } else {
      throw new NotFoundException('Could not find Entry');
    }
  }

  async deleteFoodEntry(id: number, authUser: UserEntity) {
    const food = await FoodEntity.findOne(id);
    if (food) {
      if (food.userId === authUser.id || authUser.isAdmin) {
        await FoodEntity.delete(id);
        console.log('Entry deleted Successfully');
      } else {
        throw new HttpException('Forbidden Access', 403);
      }
    } else {
      throw new NotFoundException('Could not find Entry');
    }
  }
}
