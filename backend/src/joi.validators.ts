import * as Joi from 'joi';
import { ValidationResult } from 'joi';

export const validateFoodEntry = ({
  date,
  name,
  calorie,
  price,
}): ValidationResult => {
  const Food = Joi.object({
    name: Joi.string().min(3).max(100).required(),
    calorie: Joi.number().min(50).max(3000).required(),
    price: Joi.number().min(1).max(500).required(),
    date: Joi.string().required(),
  });
  return Food.validate({ name, calorie, price, date });
};

export const validateUserEntry = ({
  name,
  dailyCalorieLimit,
  monthlyBudget,
}): ValidationResult => {
  const User = Joi.object({
    name: Joi.string().min(3).max(100).required(),
    dailyCalorieLimit: Joi.number().min(1000).max(10000).required(),
    monthlyBudget: Joi.number().min(500).max(10000).required(),
  });

  return User.validate({ name, dailyCalorieLimit, monthlyBudget });
};
