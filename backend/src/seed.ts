import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as Fs from 'fs';
import * as Faker from 'faker';
import * as _ from 'lodash';
import UserEntity from './food/user.entity';
import * as moment from 'moment';
import FoodEntity from './food/food.entity';

const bootstrap = async () => {
  try {
    await Fs.unlinkSync('src/db/data.db');
  } catch (e) {}
  const app = await NestFactory.createApplicationContext(AppModule);
  const admin = new UserEntity();
  admin.id = 1;
  admin.isAdmin = true;
  admin.name = 'Admin User';
  admin.dailyCalorieLimit = 2100;
  admin.monthlyBudget = 1000;
  await admin.save();

  const regular = new UserEntity();
  regular.id = 2;
  regular.isAdmin = false;
  regular.name = 'Regular User';
  regular.dailyCalorieLimit = 2100;
  regular.monthlyBudget = 1000;
  await regular.save();

  const users = await UserEntity.find({});
  for (let i = 0; i < 50; i++) {
    const d = moment().subtract(Math.ceil(Math.random() * 10), 'day');
    const food = new FoodEntity();
    food.name = Faker.commerce.productName();
    food.date = d.format('YYYY-MM-DD');
    food.calorie = Math.ceil(Math.random() * 900) + 100;
    food.price = Math.ceil(Math.random() * 100) + 10;
    food.userId = _.sample(users).id;
    console.log({ food });
    await food.save();
  }

  await app.close();
};

bootstrap().then();
